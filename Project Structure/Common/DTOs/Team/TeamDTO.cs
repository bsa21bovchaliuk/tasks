﻿using Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Common.DTOs.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<UserDTO> Users { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n{"createdAt:",-Constants.Indent}{CreatedAt}";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}

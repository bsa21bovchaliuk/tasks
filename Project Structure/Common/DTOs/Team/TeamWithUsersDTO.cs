﻿
using Common.DTOs.User;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.DTOs.Team
{
    public class TeamWithUsersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                + $"users:\n{Users.ToList().Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }
    }
}

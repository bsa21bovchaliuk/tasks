﻿
namespace DAL.Enums
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}

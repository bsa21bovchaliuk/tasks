﻿using DAL.Context;
using DAL.Entities.Abstract;
using DAL.UnitOfWork.Interfaces;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ProjectContext _context;

        public UnitOfWork(ProjectContext context)
        {
            _context = context;
        }

        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }

        public virtual Task SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public virtual IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

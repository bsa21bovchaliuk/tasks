﻿using Common.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDTO>> GetUsersAsync();

        Task<UserDTO> GetUserByIdAsync(int id);

        Task<UserDTO> CreateUserAsync(UserCreateDTO userCreateModel);

        Task<UserDTO> UpdateUserAsync(UserUpdateDTO userUpdateModel);

        Task DeleteUserByIdAsync(int id);

        Task<UserDetailedInfoDTO> GetUserInfo(int id);
    }
}

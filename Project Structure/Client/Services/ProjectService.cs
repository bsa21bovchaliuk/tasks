﻿using Client.Interfaces;
using Client.Properties;
using Common.ContractResolvers;
using Common.DTOs.Project;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;


namespace Client.Services
{
    public class ProjectService : IProjectService
    {
        private readonly HttpClient _client;

        public ProjectService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            var projects = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProjectByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Projects/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectDTO>();
        }

        public async Task<ProjectDTO> CreateProjectAsync(ProjectCreateDTO projectCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Resources.BaseUrl}/api/Projects", projectCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectDTO>();
        }

        public async Task<ProjectDTO> UpdateProjectAsync(ProjectUpdateDTO projectUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Resources.BaseUrl}/api/Projects", projectUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<ProjectDTO>();
        }

        public async Task DeleteProjectByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Resources.BaseUrl}/api/Projects/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<Dictionary<ProjectDTO, int>> GetProjects_TaskCountByUser(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Projects/Projects_TaskCount/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                ContractResolver = new DictionaryAsArrayResolver()
            });
        }

        public async Task<IEnumerable<ProjectDetailedInfoDTO>> GetProjectsInfo()
        {
            var projects = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Projects/DetailedInfo");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectDetailedInfoDTO>>(projects);
        }
    }
}

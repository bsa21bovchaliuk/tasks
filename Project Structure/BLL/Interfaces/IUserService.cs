﻿using Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<IEnumerable<UserDTO>> GetAllAsync();

        Task<UserDTO> GetByIdAsync(int id);

        Task<UserDTO> CreateAsync(UserCreateDTO teamDTO);

        Task<UserDTO> UpdateAsync(UserUpdateDTO teamDTO);

        Task DeleteByIdAsync(int id);

        Task<UserDetailedInfoDTO> GetUserInfo(int Id);
    }
}

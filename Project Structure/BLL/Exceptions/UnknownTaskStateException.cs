﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Exceptions
{
    public sealed class UnknownTaskStateException : Exception
    {
        public UnknownTaskStateException() : base("Unknown Task State.") { }
    }
}

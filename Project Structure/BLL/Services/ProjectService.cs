﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common;
using Common.DTOs.Project;
using Common.DTOs.Task;
using DAL.Entities;
using DAL.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        private readonly IRepository<ProjectEntity> _repository;
        private readonly IUserService _userService;
        private readonly ITeamService _teamService;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper, IUserService userService, ITeamService teamService) 
            : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<ProjectEntity>();
            _userService = userService;
            _teamService = teamService;
        }

        public async Task<ProjectDTO> CreateAsync(ProjectCreateDTO projectDTO)
        {
            var project = _mapper.Map<ProjectEntity>(projectDTO);

            await _userService.GetByIdAsync(projectDTO.AuthorId);
            await _teamService.GetByIdAsync(projectDTO.TeamId);

            project.CreatedAt = DateTime.Now;

            await _repository.CreateAsync(project);
            await _unitOfWork.SaveChangesAsync();

            var createdProject = (await GetAllAsync()).FirstOrDefault(p => p.Id == project.Id);

            return createdProject;
        }

        public async Task DeleteByIdAsync(int id)
        {
            await GetByIdAsync(id);
            await _repository.DeleteByIdAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var projects = (await _repository.GetAllAsync())
                            .Include(p => p.Team)
                            .Include(p => p.Author)
                            .Include(p => p.Tasks)
                                .ThenInclude(t => t.Performer);

            return projects.Select(project => _mapper.Map<ProjectDTO>(project))
                            .AsEnumerable();
        }

        public async Task<ProjectDTO> GetByIdAsync(int id)
        {
            var projects = await GetAllAsync();
            var project = projects.FirstOrDefault(p => p.Id == id);
                            
            if (project == null)
                throw new NotFoundException("Project", id);
            return project;
        }

        public async Task<ProjectDTO> UpdateAsync(ProjectUpdateDTO projectDTO)
        {
            var project = await _repository.GetByIdAsync(projectDTO.Id);
            if (project == null)
                throw new NotFoundException("Project", projectDTO.Id);

            await _userService.GetByIdAsync(projectDTO.AuthorId);
            await _teamService.GetByIdAsync(projectDTO.TeamId);

            project.AuthorId = projectDTO.AuthorId;
            project.TeamId = projectDTO.TeamId;
            project.Name = projectDTO.Name;
            project.Description = projectDTO.Description;
            project.Deadline = projectDTO.Deadline;

            await _repository.UpdateAsync(project);
            await _unitOfWork.SaveChangesAsync();

            return await GetByIdAsync(projectDTO.Id);
        }
        
        public async Task<Dictionary<ProjectDTO, int>> GetProjectTasksCountByUser(int userId)
        {
            await _userService.GetByIdAsync(userId);

            var projects = await GetAllAsync();

            return projects.Where(project => project.AuthorId == userId)
                            .ToDictionary(project => project, project => project.Tasks.Count);
        }

        public async Task<IEnumerable<ProjectDetailedInfoDTO>> GetProjectsInfo()
        {
            var projects = await GetAllAsync();

            return projects.Select(project => new ProjectDetailedInfoDTO
            {
                Project = project,
                LongestTask = project.Tasks.OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                ShortestTask = project.Tasks.OrderBy(task => task.Name.Length).FirstOrDefault(),
                TeamMembersCount = (project.Description.Length > Constants.MinDescriptionLength || project.Tasks.Count < Constants.MaxTasksCount) ?
                                                                 project.Team.Users.Count : default
            });
        }

        public void Dispose()
        {
            _userService.Dispose();
            _teamService.Dispose();
            _unitOfWork.Dispose();
        }
    }
}

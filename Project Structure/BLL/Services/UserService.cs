﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common;
using Common.DTOs.Project;
using Common.DTOs.Task;
using Common.DTOs.User;
using DAL.Entities;
using DAL.Enums;
using DAL.UnitOfWork.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepository<UserEntity> _repository;
        private readonly ITeamService _teamService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, ITeamService teamService) : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<UserEntity>();
            _teamService = teamService;
        }

        public async Task<UserDTO> CreateAsync(UserCreateDTO userDTO)
        {
            var user = _mapper.Map<UserEntity>(userDTO);

            if(userDTO.TeamId.HasValue)
                await _teamService.GetByIdAsync(userDTO.TeamId ?? default);
            
            user.RegisteredAt = DateTime.Now;
            await _repository.CreateAsync(user);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await GetByIdAsync(id);
            await _repository.DeleteByIdAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _repository.GetAllAsync();
            return users.Select(user => _mapper.Map<UserDTO>(user)).AsEnumerable();
        }

        public async Task<UserDTO> GetByIdAsync(int id)
        {
            var user = await _repository.GetByIdAsync(id);
            if (user == null)
                throw new NotFoundException("User", id);
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> UpdateAsync(UserUpdateDTO userDTO)
        {
            var user = await _repository.GetByIdAsync(userDTO.Id);
            if (user == null)
                throw new NotFoundException("User", userDTO.Id);

            await _teamService.GetByIdAsync(userDTO.TeamId ?? default);

            user.TeamId = userDTO.TeamId;
            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.Email = userDTO.Email;
            user.Birthday = userDTO.Birthday;

            await _repository.UpdateAsync(user);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }
        
        public async Task<UserDetailedInfoDTO> GetUserInfo(int userId)
        {
            var projectRepository = _unitOfWork.Set<ProjectEntity>();
            var taskRepository = _unitOfWork.Set<TaskEntity>();

            var users = await GetAllAsync();

            var _projects = (await projectRepository.GetAllAsync())
                            .Include(p => p.Team)
                            .Include(p => p.Author)
                            .Include(p => p.Tasks)
                                .ThenInclude(t => t.Performer)
                            .AsEnumerable();

            var _tasks = (await taskRepository.GetAllAsync())
                            .Include(t => t.Performer)
                            .AsEnumerable();

            var user = users.Where(user => user.Id == userId)
                            .Select(user => {
                                var lastProject = _projects.Where(project => project.AuthorId == user.Id)
                                                           .OrderByDescending(project => project.CreatedAt)
                                                           .FirstOrDefault();
                                var tasks = _tasks.Where(task => task.PerformerId == user.Id);
                                return new UserDetailedInfoDTO
                                {
                                    User = user,
                                    LastProject = _mapper.Map<ProjectDTO>(lastProject),
                                    LastProjectTasksCount = lastProject != null ? lastProject.Tasks.Count : default,
                                    UnfinishedTasksCount = tasks.Count(task => task.State != TaskState.Finished),
                                    LongestTask = _mapper.Map<TaskDTO>(tasks.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault())
                                };
                            })
                            .FirstOrDefault();

            if (user == null)
                throw new NotFoundException("User", userId);

            return user;
        }

        public void Dispose()
        {
            _teamService.Dispose();
            _unitOfWork.Dispose();
        }
    }
}

﻿using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAPI;
using Xunit;

namespace Web_API.Tests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }
     
        [Fact]
        public async Task DeleteById_ThenResponseCode204AndGetByIdCode404()
        {
            var userDTO = new UserCreateDTO
            {
                FirstName = "test user",
                LastName = "test",
                Birthday = DateTime.Now.AddYears(-20),
                Email = "test@gmail.com"
            };

            var jsonInString = JsonConvert.SerializeObject(userDTO);
            var response = await _client.PostAsync("api/users", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var createdUser = await response.Content.ReadAsAsync<UserDTO>();

            var deleteResponse = await _client.DeleteAsync($"api/users/{createdUser.Id}");
            var getResponse = await _client.GetAsync($"api/users/{createdUser.Id}");

            Assert.Equal(HttpStatusCode.NoContent, deleteResponse.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteById_WhenUserNotExist_ThenResponseCode404()
        {
            var deleteResponse = await _client.DeleteAsync($"api/users/{0}");
            Assert.Equal(HttpStatusCode.NotFound, deleteResponse.StatusCode);
        }

    }
}

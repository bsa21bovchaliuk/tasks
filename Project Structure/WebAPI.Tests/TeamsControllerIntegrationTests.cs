﻿using Common.DTOs.Team;
using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebAPI;
using Xunit;

namespace Web_API.Tests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task Post_ThanResponseWith201AndCorrespondedBody()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = "new team name",
            };

            var jsonInString = JsonConvert.SerializeObject(teamDTO);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            await _client.DeleteAsync($"api/teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(teamDTO.Name, createdTeam.Name);
        }

        [Fact]
        public async Task Post_WhenNameTooLong_ThenResponseCode500()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = new string('f', 101),
            };

            var jsonInString = JsonConvert.SerializeObject(teamDTO);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.InternalServerError, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetTeamsWithUsers_WhenTwoParticipantsMatchCondition_ThenTeamWithTwoUsers()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = "new team name",
            };

            var teamJsonInString = JsonConvert.SerializeObject(teamDTO);
            var teamHttpResponse = await _client.PostAsync("api/teams", new StringContent(teamJsonInString, Encoding.UTF8, "application/json"));
            var createdTeam = await teamHttpResponse.Content.ReadAsAsync<TeamDTO>();

            var userDTO = new UserCreateDTO
            {
                FirstName = "test user",
                LastName = "test",
                Birthday = DateTime.Now.AddYears(-20),
                Email = "test@gmail.com",
                TeamId = createdTeam.Id
            };

            for(int i = 0; i < 2; i++)
            {
                await _client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json"));
            }

            var httpResponse = await _client.GetAsync("api/teams/TeamsWithUsers");
            var teamWithUsers = (await httpResponse.Content.ReadAsAsync<IEnumerable<TeamWithUsersDTO>>())
                                    .FirstOrDefault(t => t.Id == createdTeam.Id);

            foreach (var user in teamWithUsers.Users)
            {
                await _client.DeleteAsync($"api/users/{user.Id}");
            }
            await _client.DeleteAsync($"api/teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(createdTeam.Name, teamWithUsers.Name);
            Assert.Equal(2, teamWithUsers.Users.Count());
        }

        [Fact]
        public async Task GetTeamsWithUsers_WhenParticipantsDontMatchCondition_ThenReturnsCollectionWithoutTeam()
        {
            var teamDTO = new TeamCreateDTO
            {
                Name = "new team name",
            };

            var teamJsonInString = JsonConvert.SerializeObject(teamDTO);
            var teamHttpResponse = await _client.PostAsync("api/teams", new StringContent(teamJsonInString, Encoding.UTF8, "application/json"));
            var createdTeam = await teamHttpResponse.Content.ReadAsAsync<TeamDTO>();

            var userDTO = new UserCreateDTO
            {
                FirstName = "test user",
                LastName = "test",
                Birthday = DateTime.Now.AddYears(-20),
                Email = "test@gmail.com",
                TeamId = createdTeam.Id
            };

            for (int i = 0; i < 2; i++)
            {
                await _client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json"));
            }

            userDTO.Birthday = DateTime.Now;
            await _client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json"));

            var httpResponse = await _client.GetAsync("api/teams/TeamsWithUsers");
            var teamWithUsers = (await httpResponse.Content.ReadAsAsync<IEnumerable<TeamWithUsersDTO>>())
                                    .FirstOrDefault(t => t.Id == createdTeam.Id);

            var getTeamHttpResponse = await _client.GetAsync($"api/teams/{createdTeam.Id}");
            var team = await getTeamHttpResponse.Content.ReadAsAsync<TeamDTO>();

            foreach (var user in team.Users)
            {
                await _client.DeleteAsync($"api/users/{user.Id}");
            }
            await _client.DeleteAsync($"api/teams/{team.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Null(teamWithUsers);
        }
    }
}
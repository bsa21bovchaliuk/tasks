﻿using AutoMapper;
using BLL.Interfaces;
using BLL.MappingProfiles;
using BLL.Services;
using Common;
using Common.DTOs.Team;
using DAL.Entities;
using DAL.UnitOfWork.Implementation;
using DAL.UnitOfWork.Interfaces;
using FakeItEasy;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BLL.Tests
{
    public class TeamServiceTests : IClassFixture<SeedDataFixture>, IDisposable
    {
        private readonly ITeamService _teamService;
        private readonly Mapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly SeedDataFixture _fixture;

        public TeamServiceTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
            }));
            _fixture = new SeedDataFixture();

            _unitOfWork = new UnitOfWork(_fixture.Context);

            _teamService = new TeamService(_unitOfWork, _mapper);
        }

        public void Dispose()
        {
            _fixture.Dispose();
            _unitOfWork.Dispose();
            _teamService.Dispose();
        }

        [Fact]
        public async Task GetUsersGroupedByTeam_WhenOneTeamWithAllUsersMatchCondition_ThenResultContainsOneElement()
        {
            await _teamService.CreateAsync(A.Fake<TeamCreateDTO>());
            await _teamService.CreateAsync(A.Fake<TeamCreateDTO>());

            var userRepository = _unitOfWork.Set<UserEntity>();

            {
                var user = A.Fake<UserEntity>();
                user.TeamId = 1;
                user.Birthday = DateTime.Now;
                await userRepository.CreateAsync(user);
            }

            for (int i = 0; i < 5; i++)
            {
                var user = A.Fake<UserEntity>();
                user.TeamId = 1;
                user.Birthday = DateTime.Now.AddYears(-(Constants.MinAge + 1));
                await userRepository.CreateAsync(user);
            }

            for (int i = 0; i < 5; i++)
            {
                var user = A.Fake<UserEntity>();
                user.TeamId = 2;
                user.Birthday = DateTime.Now.AddYears(-(Constants.MinAge + 1));
                await userRepository.CreateAsync(user);
            }

            await _unitOfWork.SaveChangesAsync();

            var result = await _teamService.GetTeamsWithUsers();

            Assert.Single(result);
            Assert.Equal(5, result.FirstOrDefault().Users.Count());
        }

        [Fact]
        public async Task GetUsersGroupedByTeam_WhenOneTeamWithAllUsersMatchCondition_ThenFisrtUserInTeamIsLastRegistered()
        {

            await _teamService.CreateAsync(A.Fake<TeamCreateDTO>());

            var userRepository = _unitOfWork.Set<UserEntity>();

            var registerDate = DateTime.Now.AddDays(-30);

            for (int i = 0; i < 5; i++)
            {
                var user = A.Fake<UserEntity>();
                user.TeamId = 1;
                user.Birthday = DateTime.Now.AddYears(-(Constants.MinAge + 1));
                user.RegisteredAt = registerDate.AddDays(i);
                await userRepository.CreateAsync(user);
            }

            await _unitOfWork.SaveChangesAsync();

            var result = await _teamService.GetTeamsWithUsers();

            Assert.Equal(5, result.FirstOrDefault().Users.FirstOrDefault().Id);
        }
    }
}
